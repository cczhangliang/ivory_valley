package com.ivory.valley;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import com.ivory.valley.common.IvoryValley;
import com.ivory.valley.common.SignAttr;

import net.sf.json.JSONObject;

public class IvoryUtils {
	
	public static void main(String[] args) {
		//checkAccessToken();
		httpGetAuth(IvoryValley.ConstantAndURL.Auth_url,IvoryValley.ConstantAndURL.appid,"http://xyg.bigdatas.top/ivory-valley/auth/auth.dx","123");
	}
	//校验accessToken是否过期
	public  void checkAccessToken(){
		long curTime = System.currentTimeMillis()/1000;
		long expiresTime = SignAttr.Access_Token_Time+SignAttr.Access_Token_ExpiresIn;
		if(expiresTime == 0 || expiresTime>curTime){//没有值或者过期
			httpGetToken(IvoryValley.ConstantAndURL.appid,IvoryValley.ConstantAndURL.secret,IvoryValley.ConstantAndURL.Access_Token_url);
		}
	}
	
	//校验ticket是否过期
		public  void checkTicket(){
			long curTime = System.currentTimeMillis()/1000;
			long expiresTime = SignAttr.Jsapi_Ticket_Time+SignAttr.Jsapi_Ticket_ExpiresIn;
			if(expiresTime == 0 || expiresTime>curTime){//没有值或者过期就重新获取
				httpGetTicket(SignAttr.Access_Token,IvoryValley.ConstantAndURL.JsapiTicket_url);
			}
		}
		
	public static void httpGetAuth(String url, String appid,String redirect_uri,String state){
		try {
			url = url.replaceAll("APPID", appid).replaceAll("REDIRECT_URI", URLEncoder.encode(redirect_uri, "utf-8")).replaceAll("STATE", state);
			URL urlGet = new URL(url);
			HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			http.connect();
			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			String message = new String(jsonBytes,"UTF-8");
			JSONObject json = JSONObject.fromObject(message);
			String code = json.getString("code");
			System.out.println("code==========================="+code);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 获取token
	 * @param appid
	 * @param secret
	 * @param url
	 */
	public  void httpGetToken(String appid,String secret,String url){
		url = url.replaceAll("APPID", appid).replaceAll("APPSECRET", secret);
		String accessToken = null;
	    String expiresIn = null;
		try {
			URL urlGet = new URL(url);
			HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			http.connect();
			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			String message = new String(jsonBytes,"UTF-8");
			JSONObject json = JSONObject.fromObject(message);
			accessToken = json.getString("access_token");
			expiresIn = json.getString("expires_in");
			//保存access_token到本地
			SignAttr.Access_Token = accessToken;
			SignAttr.Access_Token_Time = System.currentTimeMillis()/1000;
			SignAttr.Access_Token_ExpiresIn = Integer.valueOf(expiresIn);
			
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 获取ticket
	 * @param access_token
	 * @param url
	 */
	public  void httpGetTicket(String access_token,String url){
		url = url.replaceAll("ACCESS_TOKEN", access_token);
		String ticket = null;
	    String expiresIn = null;
		try {
			URL urlGet = new URL(url);
			HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			http.connect();
			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			String message = new String(jsonBytes,"UTF-8");
			JSONObject json = JSONObject.fromObject(message);
			ticket = json.getString("ticket");
			expiresIn = json.getString("expires_in");
			//更新ticket到全局
			SignAttr.Jsapi_Ticket = ticket;
			SignAttr.Jsapi_Ticket_Time = System.currentTimeMillis()/1000;
			SignAttr.Jsapi_Ticket_ExpiresIn = Integer.valueOf(expiresIn);
			
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
