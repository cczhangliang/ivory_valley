package com.ivory.valley.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ivory.valley.common.IvorySession;
import com.ivory.valley.session.SessionContainer;




public class SessionUtil {

	private static SessionContainer<IvorySession> sessionContainer =new SessionContainer<>();


	public static void setLogined(HttpServletRequest request, HttpServletResponse response, IvorySession session) {
		sessionContainer.set(request, response, session);
	}

	public static void updateLogined(HttpServletRequest request, HttpServletResponse response,
			IvorySession session) {
		sessionContainer.update(request, response, session);
	}

	public static IvorySession getLogined(HttpServletRequest request, HttpServletResponse response) {
		return sessionContainer.get(request, response);
	}

	public static boolean isLogined(HttpServletRequest request, HttpServletResponse response) {
		return sessionContainer.has(request, response);
	}
}
