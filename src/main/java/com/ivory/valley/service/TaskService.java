package com.ivory.valley.service;

import java.util.List;
import java.util.Map;

import com.ivory.valley.common.Resp;
import com.ivory.valley.pojo.Task;


public interface TaskService {

	int insertTask(Task task);

	Resp<List<Task>> selectAllTask(Map<String, Object> map);

}
