/**
 * 
 */
package com.ivory.valley.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ivory.valley.common.Resp;
import com.ivory.valley.dao.TaskDetailsMongo;
import com.ivory.valley.dao.TaskMapper;
import com.ivory.valley.pojo.Task;
import com.ivory.valley.pojo.TaskDetails;
import com.ivory.valley.service.TaskService;

/**
 * @author zhang
 *
 */
@Service
@Transactional
public class TaskServiceImpl implements TaskService {
	
	@Autowired
	private TaskMapper taskMapper;
	
	@Autowired
	private TaskDetailsMongo taskDetailsMongo;
	
	@Override
	public int insertTask(Task Task) {
		int index = taskMapper.insertSelective(Task);
		return index;
	}

	@Override
	public Resp<List<Task>> selectAllTask(Map<String, Object> map) {
		Resp<List<Task>> resp = new Resp<>();
		List<Task> list = taskMapper.selectAllTask(map);
		for(Task task : list){
			String taskDetailId = task.getTask_detail_id();
			List<TaskDetails> dlist = taskDetailsMongo.queryTaskDetails(taskDetailId);
			task.setTask_details(dlist);
		}
		resp.setRes(list);
		resp.setSuccessCode();
		return resp;
	}

}
