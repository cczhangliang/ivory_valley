/**
 * 
 */
package com.ivory.valley.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ivory.valley.dao.UserMapper;
import com.ivory.valley.pojo.User;
import com.ivory.valley.service.UserService;

/**
 * @author zhang
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public User insetORupdate(User user) {
		userMapper.insertORupdate(user) ;
		user = userMapper.selectByOpenid(user.getOpenid());
		return user;
	}
}
