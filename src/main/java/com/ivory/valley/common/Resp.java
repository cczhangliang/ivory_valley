package com.ivory.valley.common;

import com.ivory.valley.pojo.BaseBean;

public class Resp<T> extends BaseBean {

	private static final long serialVersionUID = 1L;
	private int code = IvoryValley.MsgCode.Unknow;
	private String msg;

	private T res;

	public Resp() {
		super();
	}

	public Resp(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public Resp(int code, String msg, T res) {
		super();
		this.code = code;
		this.msg = msg;
		this.res = res;
	}

	public T getRes() {
		return res;
	}

	public void setRes(T res) {
		this.res = res;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setCodeMsg(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public void setSuccessCode() {
		this.code = IvoryValley.MsgCode.Success;
	}

	public void setFailCode() {
		this.code = IvoryValley.MsgCode.Failure;

	}

	public void setSuccessCodeMsg(String msg) {
		this.code = IvoryValley.MsgCode.Success;
		this.msg = msg;
	}

	public void setFailCodeMsg(String msg) {
		this.code = IvoryValley.MsgCode.Failure;
		this.msg = msg;
	}

}
