package com.ivory.valley.common;

import com.ivory.valley.session.SessionObject;

public class IvorySession extends SessionObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String openid;//用户的唯一标识
	
	private String nickname;//用户昵称
	
	private String sex;//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	
	private String province;//省份
	
	private String city;//城市
	
	private String country;//国家
	
	private String headimgurl;//用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	
	private String privilege;//用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	
	private String unionid;//只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段
	
	private long checkExpireTime ;//用code获取accessToken的时间，验证过期时间
	/**
	 * @param userId
	 */
	public IvorySession(Integer userId) {
		super(userId);
	}
	/**
	 * @return the openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * @param openid the openid to set
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the headimgurl
	 */
	public String getHeadimgurl() {
		return headimgurl;
	}
	/**
	 * @param headimgurl the headimgurl to set
	 */
	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}
	/**
	 * @return the privilege
	 */
	public String getPrivilege() {
		return privilege;
	}
	/**
	 * @param privilege the privilege to set
	 */
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	/**
	 * @return the unionid
	 */
	public String getUnionid() {
		return unionid;
	}
	/**
	 * @param unionid the unionid to set
	 */
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	/**
	 * @return the checkExpireTime
	 */
	public long getCheckExpireTime() {
		return checkExpireTime;
	}
	/**
	 * @param checkExpireTime the checkExpireTime to set
	 */
	public void setCheckExpireTime(long checkExpireTime) {
		this.checkExpireTime = checkExpireTime;
	}
	
	

}
