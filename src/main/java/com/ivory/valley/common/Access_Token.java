package com.ivory.valley.common;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;



import net.sf.json.JSONObject;

public class Access_Token {

	public  void httpGetToken(String appid,String secret,String url){
		url = url.replaceAll("APPID", appid).replaceAll("APPSECRET", secret);
		String accessToken = null;
	    String expiresIn = null;
		try {
			URL urlGet = new URL(url);
			HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);
			http.connect();
			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] jsonBytes = new byte[size];
			is.read(jsonBytes);
			String message = new String(jsonBytes,"UTF-8");
			JSONObject json = JSONObject.fromObject(message);
			accessToken = json.getString("access_token");
			expiresIn = json.getString("expires_in");
			SignAttr.Access_Token = accessToken;
			SignAttr.Access_Token_Time = System.currentTimeMillis()/1000;
			SignAttr.Access_Token_ExpiresIn = Integer.valueOf(expiresIn);
			//保存access_token到本地
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
