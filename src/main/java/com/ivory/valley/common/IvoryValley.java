package com.ivory.valley.common;


import org.springframework.context.ApplicationContext;

public abstract  class IvoryValley {
	
	/**
	 * 消息状态码
	 */
	public interface MsgCode{
		public final int Unknow=-9999; //系统示知错误
		public final int SessionExpire=-9998;	//会话过期
		public final int FieldError=-9997;	//字段规则验证错误
				
		public final int Failure=-1;	//失败默认值
		public final int Success=0;		//成功默认值

	}
	public interface PageSize{
		public final int size = 5;
	}
	public interface ConstantAndURL{
		public static final String  appid = "wx7491574828fade87";
		public static final String secret = "578d18779ec51d6708a806863ed3a7bb";
		public static final String mch_id="1469701602";//商户号
		
		public static String get_media_url = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
		public static String Access_Token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
		public static String JsapiTicket_url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
		public static String Auth_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
	}
	
	public interface AttrName{		
		public static final String RESP="resp";	//返回结果属性名
	}
	
	public interface CookieName{
		public static final String SessionFlag="sessionFlag";	//会话Cookie标识
	}	



	private static ApplicationContext applicationContext;

	public final static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(ApplicationContext applicationContext) {
		IvoryValley.applicationContext = applicationContext;
	}
}
