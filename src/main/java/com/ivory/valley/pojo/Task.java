package com.ivory.valley.pojo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Task extends BaseBean {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private Integer receive_task_userId;

    private Integer publish_task_userId;

    private String task_title;

    private String key_words;
    
	private String address;

    private String task_classification;

    private BigDecimal prepaid_bounty;

    private Byte receive_status;

    private Byte task_execute_status;
    
    private String task_detail_id;

    private Date dead_line;

    private Date publish_task_time;

    private Date task_receive_time;

    private Date update_task_time;

    private Date task_finish_time;

    private Short task_finish_condition;
    
    private List<TaskDetails> task_details;
    
    public String getAddress() {
  		return address;
  	}

  	public void setAddress(String address) {
  		this.address = address;
  	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReceive_task_userId() {
        return receive_task_userId;
    }

    public void setReceive_task_userId(Integer receive_task_userId) {
        this.receive_task_userId = receive_task_userId;
    }

    public Integer getPublish_task_userId() {
        return publish_task_userId;
    }

    public void setPublish_task_userId(Integer publish_task_userId) {
        this.publish_task_userId = publish_task_userId;
    }

    public String getTask_title() {
        return task_title;
    }

    public void setTask_title(String task_title) {
        this.task_title = task_title;
    }

    public String getKey_words() {
        return key_words;
    }

    public void setKey_words(String key_words) {
        this.key_words = key_words;
    }

    public String getTask_classification() {
        return task_classification;
    }

    public void setTask_classification(String task_classification) {
        this.task_classification = task_classification;
    }

    public BigDecimal getPrepaid_bounty() {
        return prepaid_bounty;
    }

    public void setPrepaid_bounty(BigDecimal prepaid_bounty) {
        this.prepaid_bounty = prepaid_bounty;
    }

    public Byte getReceive_status() {
        return receive_status;
    }

    public void setReceive_status(Byte receive_status) {
        this.receive_status = receive_status;
    }

    public Byte getTask_execute_status() {
        return task_execute_status;
    }

    public void setTask_execute_status(Byte task_execute_status) {
        this.task_execute_status = task_execute_status;
    }

    public Date getDead_line() {
        return dead_line;
    }

    public void setDead_line(Date dead_line) {
        this.dead_line = dead_line;
    }

    public Date getPublish_task_time() {
        return publish_task_time;
    }

    public void setPublish_task_time(Date publish_task_time) {
        this.publish_task_time = publish_task_time;
    }

    public Date getTask_receive_time() {
        return task_receive_time;
    }

    public void setTask_receive_time(Date task_receive_time) {
        this.task_receive_time = task_receive_time;
    }

    public Date getUpdate_task_time() {
        return update_task_time;
    }

    public void setUpdate_task_time(Date update_task_time) {
        this.update_task_time = update_task_time;
    }

    public Date getTask_finish_time() {
        return task_finish_time;
    }

    public void setTask_finish_time(Date task_finish_time) {
        this.task_finish_time = task_finish_time;
    }

    public Short getTask_finish_condition() {
        return task_finish_condition;
    }

    public void setTask_finish_condition(Short task_finish_condition) {
        this.task_finish_condition = task_finish_condition;
    }

	public String getTask_detail_id() {
		return task_detail_id;
	}

	public void setTask_detail_id(String task_detail_id) {
		this.task_detail_id = task_detail_id;
	}

	/**
	 * @return the task_details
	 */
	public List<TaskDetails> getTask_details() {
		return task_details;
	}

	/**
	 * @param task_details the task_details to set
	 */
	public void setTask_details(List<TaskDetails> task_details) {
		this.task_details = task_details;
	}


}