package com.ivory.valley.pojo;

import java.lang.reflect.Field;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bson.types.ObjectId;

import com.ivory.valley.util.SecurityUtil;


public class MongoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ObjectId _id;

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	/**
	 * 数据掩码,用于判断当前数据是否更改 掩码计算规则：
	 * 除(ObjectId类型、createTime、updateTime)外,其它字段都参与计算
	 * 
	 * @return
	 */
	public String getDataMask() {
		return SecurityUtil.md5(new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE) {
			protected boolean accept(Field f) {
				return super.accept(f) && f.getType() != ObjectId.class && 
						!"createTime".equals(f.getName()) && !"updateTime".equals(f.getName()) && !"expireTime".equals(f.getName());
			}
		}.toString(), "");
	}

	/**
	 * 唯一标识掩码,用于判断是否同一条数据 掩码计算规则：选取能标识有别于其它数据的不变字段参与计算
	 * 
	 * @return
	 */
	public String getUniqueFlag() {
		return null;
	}
}
