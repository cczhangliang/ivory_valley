package com.ivory.valley.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class IncomepayFlow extends BaseBean {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private Integer user_id;

    private Integer task_record_id;

    private BigDecimal income;

    private BigDecimal pay;

    private BigDecimal banalces;

    private Date create_time;

    private Short transaction_type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getTask_record_id() {
        return task_record_id;
    }

    public void setTask_record_id(Integer task_record_id) {
        this.task_record_id = task_record_id;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    public BigDecimal getBanalces() {
        return banalces;
    }

    public void setBanalces(BigDecimal banalces) {
        this.banalces = banalces;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Short getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(Short transaction_type) {
        this.transaction_type = transaction_type;
    }
}