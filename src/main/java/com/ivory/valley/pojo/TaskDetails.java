package com.ivory.valley.pojo;

public class TaskDetails extends MongoBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String task_detail_id;//任务详情id
	
	private String type;//详情类型:1文本2图片3音频
	
	private String index;//长度
	
	private String text_content;//文本内容
	
	private byte media_bytes;//图片音频base64编码
	
	

	/**
	 * @return the task_detail_id
	 */
	public String getTask_detail_id() {
		return task_detail_id;
	}

	/**
	 * @param task_detail_id the task_detail_id to set
	 */
	public void setTask_detail_id(String task_detail_id) {
		this.task_detail_id = task_detail_id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the index
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(String index) {
		this.index = index;
	}

	/**
	 * @return the text_content
	 */
	public String getText_content() {
		return text_content;
	}

	/**
	 * @param text_content the text_content to set
	 */
	public void setText_content(String text_content) {
		this.text_content = text_content;
	}

	/**
	 * @return the media_bytes
	 */
	public byte getMedia_bytes() {
		return media_bytes;
	}

	/**
	 * @param media_bytes the media_bytes to set
	 */
	public void setMedia_bytes(byte media_bytes) {
		this.media_bytes = media_bytes;
	}
	
	 
	
}
