package com.ivory.valley.dao;

import com.ivory.valley.pojo.User;

public interface UserMapper extends BaseMapper{
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

	int insertORupdate(User user);

	User selectByOpenid(String openid);
}