package com.ivory.valley.dao;

import com.ivory.valley.pojo.IncomepayFlow;

public interface IncomepayFlowMapper extends BaseMapper{
    int deleteByPrimaryKey(Integer id);

    int insert(IncomepayFlow record);

    int insertSelective(IncomepayFlow record);

    IncomepayFlow selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(IncomepayFlow record);

    int updateByPrimaryKey(IncomepayFlow record);
}