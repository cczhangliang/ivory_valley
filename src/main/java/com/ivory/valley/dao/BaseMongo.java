package com.ivory.valley.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.DB;
import com.mongodb.DBCollection;


public abstract class BaseMongo extends AbstractMongo{

	@Autowired
	private MongoTemplate mongoTemplate;

	protected MongoTemplate getMongoTemplate(){
		return mongoTemplate;
	}
	
	protected DB getDB(){
		return this.mongoTemplate.getDb();
	}
	
	protected DBCollection getCollection(String collection){
		return this.getDB().getCollection(collection);
	}

}
