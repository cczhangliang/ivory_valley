package com.ivory.valley.dao;

import java.util.List;
import java.util.Map;

import com.ivory.valley.pojo.Task;

public interface TaskMapper extends BaseMapper{
    int deleteByPrimaryKey(Integer id);

    int insert(Task record);

    int insertSelective(Task record);

    Task selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Task record);

    int updateByPrimaryKey(Task record);

	List<Task> selectAllTask(Map<String, Object> map);
}