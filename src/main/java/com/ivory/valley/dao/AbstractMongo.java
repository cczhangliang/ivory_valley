package com.ivory.valley.dao;

import java.util.HashSet;
import java.util.Set;

import com.ivory.valley.pojo.MongoBean;
import com.ivory.valley.util.BeanUtil;
import com.mongodb.DBObject;

public abstract class AbstractMongo {

	// 集合名字
	public interface IName {
		
		final String ivory_valley = "ivory-valley";

	}

	public <T extends MongoBean> DBObject bean2DBObject(T bean, boolean containNull) {
		return bean2DBObject(bean, containNull, false);
	}

	public <T extends MongoBean> DBObject bean2DBObject(T bean, boolean containNull, boolean containMaskCode) {
		Set<String> exclusive = null;
		if (!containMaskCode) {
			exclusive = new HashSet<String>();
			exclusive.add("dataMask");
		}
		return BeanUtil.bean2DBObject(bean, containNull, exclusive);
	}

	public <T extends MongoBean> T dbObject2Bean(DBObject dbObject, Class<T> type) {
		try {
			return BeanUtil.dbObject2Bean(dbObject, type.newInstance());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
