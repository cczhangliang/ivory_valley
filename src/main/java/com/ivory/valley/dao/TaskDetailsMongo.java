package com.ivory.valley.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ivory.valley.pojo.TaskDetails;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

@Repository
public class TaskDetailsMongo extends BaseMongo {

	@SuppressWarnings("unused")
	public boolean insert(List<BasicDBObject> list ){
		WriteResult wr = this.getDB().getCollection("task_details").insert(list);
		return wr.getN()>0 ?true:false;
	}
	
	public List<TaskDetails> queryTaskDetails(String taskDetailId){
		DBObject query = new BasicDBObject();
		List<TaskDetails> list = new ArrayList<TaskDetails>();
		query.put("task_detail_id", taskDetailId);
		DBCursor cursor = this.getDB().getCollection("task_details").find(query);
		if(cursor != null){
			while(cursor.hasNext()){
				TaskDetails taskDetail = this.dbObject2Bean(cursor.next(), TaskDetails.class);
				list.add(taskDetail);
			}
		}
		return list;
	}
}
