package com.ivory.valley.web;

import org.springframework.web.context.WebApplicationContext;

import com.ivory.valley.common.IvoryValley;


public class DispatcherServlet extends org.springframework.web.servlet.DispatcherServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected WebApplicationContext initWebApplicationContext() {
		WebApplicationContext wac=super.initWebApplicationContext();
		IvoryValley.setApplicationContext(wac);
		return wac;
	}
}
