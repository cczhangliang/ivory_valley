package com.ivory.valley.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;

import org.springframework.web.filter.CharacterEncodingFilter;

@WebFilter(urlPatterns={ "/*"})
public class Filter01Encoding extends CharacterEncodingFilter {

	protected void initFilterBean() throws ServletException {
		this.setEncoding("UTF-8");
		super.initFilterBean();
	}
}
