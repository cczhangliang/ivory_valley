package com.ivory.valley.session;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivory.valley.controller.AuthController;
import com.ivory.valley.util.SecurityUtil;



public class SessionContainer<T extends SessionObject> {
	private Logger logger = LoggerFactory.getLogger(SessionContainer.class);
	private Map<String,T> map = new HashMap<String,T>();
	//private KVCache<T> sessionCache;
	private final String cookieName="sessionFlag";
	private String cookiePath;
	private int timeout = 60*30;	
	

	
	public void set(HttpServletRequest request, HttpServletResponse response, T sessionObject){
		if(sessionObject!=null){
			logger.info("sessionObject="+sessionObject);
			String sessionId = generateSessionId(request,sessionObject.getUserId());
			sessionObject.setSessionId(sessionId);
			//sessionCache.put(sessionId, sessionObject, timeout);
			map.put(sessionId, sessionObject);
			setCookie(request, response, sessionId);
		}else{
			this.remove(request, response);
		}
	}

	public void update(HttpServletRequest request, HttpServletResponse response, T sessionObject){		
		if(sessionObject!=null){
			String sessionId=this.getCookie(request);
			if(StringUtils.isNotBlank(sessionId)){
				//sessionCache.update(sessionId, sessionObject, timeout);
				map.put(sessionId, sessionObject);
			}			
		}else{
			this.remove(request, response);
		}
	}

	public T get(HttpServletRequest request, HttpServletResponse response){
		String sessionId=this.getCookie(request);		
		if(StringUtils.isBlank(sessionId)){
			return null;
		}
		return map.get(sessionId);
	}

	public boolean has(HttpServletRequest request, HttpServletResponse response){
		return this.get(request, response)!=null;
	}
	
	private void remove(HttpServletRequest request, HttpServletResponse response){
		String sessionId=this.getCookie(request);
		if(StringUtils.isNotBlank(sessionId)){
			//sessionCache.del(sessionId);
			map.remove(sessionId);
			setCookie(request, response, null);
		}		
	}
	
	//----------
	private String generateSessionId(HttpServletRequest request, long userId){
		String sessionId=null;
		int count=0;
		do{
			sessionId=SecurityUtil.md5(String.valueOf(userId+"%"+(++count)), "YjRiNzU=");	
		}while(map.containsKey(sessionId));
		return sessionId;
	}
	
	private String getCookie(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		if(cookies!=null)
		for(Cookie cookie : cookies){
			if(cookieName.equals(cookie.getName())){
				return cookie.getValue();
			}
		}
		return null;
	}
	
	private void setCookie(HttpServletRequest request, HttpServletResponse response, String sessionId){
		Cookie cookie = new Cookie(cookieName,sessionId);
		cookie.setPath(this.cookiePath==null ? request.getContextPath() : this.cookiePath);
		cookie.setMaxAge(sessionId==null ? 0 : timeout);
		response.addCookie(cookie);
	}	
}
