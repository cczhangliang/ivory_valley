package com.ivory.valley.session;

import com.ivory.valley.pojo.BaseBean;

public class SessionObject extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sessionId;
	private Integer userId;
	
	public SessionObject() {
		super();
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public SessionObject(Integer userId) {
		super();
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
