package com.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ivory.valley.common.IvoryValley;
import com.ivory.valley.dao.TaskDetailsMongo;
import com.ivory.valley.dao.TaskMapper;
import com.ivory.valley.dao.UserMapper;
import com.ivory.valley.pojo.Task;
import com.ivory.valley.pojo.TaskDetails;
import com.ivory.valley.pojo.User;

public class Test {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml","ivory_valley_servlet.xml");
		IvoryValley.setApplicationContext(context);
		TaskMapper taskMapper = (TaskMapper) context.getBean("taskMapper");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("receive_status", 0);
		List<Task> list = taskMapper.selectAllTask(map);
		//List<TaskDetails> list = taskDetailsMongo.queryTaskDetails("1495711759221");
		System.out.println(list.toString());
	}

}
