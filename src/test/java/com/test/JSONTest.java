package com.test;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ivory.valley.controller.TaskController;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class JSONTest {

	private static Log log = LogFactory.getLog(JSONTest.class);
	public static void main(String[] args) {

		String contents = "[{'textShow':true,'showBottom':false,'contents':{'textContent':'111','type':1,'index':0}},{'textShow':true,'showBottom':false,'contents':{'textContent':'222','type':1,'index':1}}]";
		JSONArray jsonArrayContents = JSONArray.fromObject(contents);
		for(int i=0;i<jsonArrayContents.size();i++){
			JSONObject jsonObj = jsonArrayContents.getJSONObject(i);
			String content = jsonObj.getString("contents");
			JSONObject contentObj = JSONObject.fromObject(content);
			String type = contentObj.getString("type");
			log.info(type);
		}
		
	}

}
